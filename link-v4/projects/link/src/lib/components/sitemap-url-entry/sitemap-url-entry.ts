import { Component, ViewChild, Input, Output, EventEmitter } from '@angular/core';

import { DevLogger as dl } from '@ngcore/core'; import isDL = dl.isLoggable;
import { DateTimeUtil, DateIdUtil } from '@ngcore/core';

import { ChangeFrequency } from '../../sitemap/core/change-frequency';


@Component({
  selector: 'sitemap-url-entry',
  template: `<url>
  <loc>{{loc}}</loc>
  <lastmod>{{lastMod}}</lastmod>
  <changefreq>{{changeFreq}}</changefreq>
  <priority>{{priority}}</priority>
</url>
`
})
export class SitemapUrlEntryComponent {
  // @Input("iid") itemId: string;
  @Input("location") location: string = '';
  @Input("last-modified") lastModified: number = 0;
  @Input("change-frequency") changeFrequency: ChangeFrequency = ChangeFrequency.never;  // tbd: use number or string???
  @Input("page-priority") pagePriority: number = 0;
  @Input("exclude-time") excludeTime: boolean = false
  
  constructor(
  ) {
    // if(isDL()) dl.log('Hello SitemapUrlEntry Component. id = ' + this.navCtrl.id);
  }

  ngOnInit() {
    // TBD:
  }


  get loc(): string {
    return this.location;
  }
  get lastMod(): string {
    return DateTimeUtil.getISODateTimeString(this.lastModified, this.excludeTime);
  }
  get changeFreq(): string {
    return ChangeFrequency[this.changeFrequency];
  }
  get priority(): string {
    if(this.pagePriority <= 0) {
      return "0";
    } else if(this.pagePriority >= 1.0) {
      return "1.0";
    }
    let truncated = Math.floor(this.pagePriority * 10) / 10;
    return truncated.toString();
  }

}
