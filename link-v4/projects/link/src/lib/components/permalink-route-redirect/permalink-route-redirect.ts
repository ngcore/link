import { Component, EventEmitter } from '@angular/core';

import { DevLogger as dl } from '@ngcore/core'; import isDL = dl.isLoggable;
import { DateTimeUtil, DateIdUtil } from '@ngcore/core';

import { PermalinkPathUtil } from '../../permalink/util/permalink-path-util';
import { PermalinkManagerService } from '../../services/permalink-manager.service'


@Component({
  selector: 'permalink-route-redirect',
  template: ``
})
export class PermalinkRouteRedirectComponent {
  
  constructor(
    private permalinkManagerService: PermalinkManagerService,
  ) {
  }

  ngOnInit() {
    // TBD:
  }

}
