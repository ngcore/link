import { Component, ViewChild, Input, Output, EventEmitter } from '@angular/core';

import { DevLogger as dl } from '@ngcore/core'; import isDL = dl.isLoggable;
import { DateTimeUtil, DateIdUtil } from '@ngcore/core';

import { ChangeFrequency } from '../../sitemap/core/change-frequency';
import { SiteEntry } from '../../sitemap/model/site-entry';


@Component({
  selector: 'sitemap-site-entry',
  template: `<url>
  <loc>{{siteEntry.loc}}</loc>
  <lastmod>{{siteEntry.lastMod}}</lastmod>
  <changefreq>{{siteEntry.changeFreq}}</changefreq>
  <priority>{{siteEntry.priority}}</priority>
</url>
`
})
export class SitemapSiteEntryComponent {
  // @Input("iid") itemId: string;
  @Input("site-entry") siteEntry: SiteEntry;
  
  constructor(
  ) {
    // if(isDL()) dl.log('Hello SitemapSiteEntry Component. id = ' + this.navCtrl.id);
    this.siteEntry = new SiteEntry();
  }

  ngOnInit() {
    // TBD:
  }

}
