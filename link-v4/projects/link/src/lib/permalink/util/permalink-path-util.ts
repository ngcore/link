import { DevLogger as dl } from '@ngcore/core'; import isDL = dl.isLoggable;

export namespace PermalinkPathUtil {

  // tbd:
  // Need to (possibly) URL encode
  //  and keep it under a maximum length, etc.
  // Note that we assume that the uinqueId param ensures uniqueness,
  //   and hence this function does not try to create a unique Url in any way.
  // The returned path does not include a leading slash.
  export function getPermalinkPath(uniqueId: string, title: string, description: (string | null) = null): string {
    let path = '';
    if(uniqueId) {
      if(uniqueId.startsWith('/')) {
        uniqueId = uniqueId.substring(1);
      }
      if(uniqueId) {
        path = uniqueId;
      }
    }
    if(title) {
      title = title.replace(/[^A-Z0-9]/ig, " ").trim().replace(/\s+/g, '-').toLowerCase();
      path += '-' + title;  // Note that we add leading '-' even if uniqueId == ''.
    }
    if(description) {  // TBD: Truncate if it's too long?
      description = description.replace(/[^A-Z0-9]/ig, " ").trim().replace(/\s+/g, '-').toLowerCase();
      path += '-' + description;  // Again, we add '-' regardless of whether uniqueId/title is empty.
    }
    return path;
  }
  // "Reverse" of getPermalinkPath()
  export function getUniqueId(permalinkPath: string): string {
    let uniqueId = '';
    if(permalinkPath) {
      if(permalinkPath.startsWith('/')) {
        permalinkPath = permalinkPath.substring(1);
      }
      if(permalinkPath) {
        let x = permalinkPath.indexOf('-');
        if(x >= 0) {  // x==0 means uniqueId == ''
          uniqueId = permalinkPath.substring(0, x);
        } else {
          uniqueId = permalinkPath;  // ??? Or, just return ''??
        }
      }
    }
    return uniqueId;
  }


  // TBD:
  // We need a method to create a unique path (with a reasonably small chance of collision)
  //   e.g., using random strings, etc....
  // We need one-way unique mapping: e.g., args -> path mapping should be deterministic.
  export function getRandomUniquePath(prefix: string, title: string, description: (string | null) = null): string {
    return '';
  }

}
