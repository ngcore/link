import { NgModule, ModuleWithProviders, ErrorHandler } from '@angular/core';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NgCoreCoreModule } from '@ngcore/core';
import { NgCoreIdleModule } from '@ngcore/idle';

import { CommonLinkService } from './services/common-link.service';
import { PermalinkManagerService } from './services/permalink-manager.service';
import { SitemapUrlEntryComponent } from './components/sitemap-url-entry/sitemap-url-entry';
import { SitemapSiteEntryComponent } from './components/sitemap-site-entry/sitemap-site-entry';
import { PermalinkRouteRedirectComponent } from './components/permalink-route-redirect/permalink-route-redirect';


@NgModule({
  schemas: [ NO_ERRORS_SCHEMA ],
  imports: [
    CommonModule,
    NgCoreCoreModule.forRoot(),
    NgCoreIdleModule.forRoot()
  ],
  declarations: [
    SitemapUrlEntryComponent,
    SitemapSiteEntryComponent,
    PermalinkRouteRedirectComponent
  ],
  exports: [
    SitemapUrlEntryComponent,
    SitemapSiteEntryComponent,
    PermalinkRouteRedirectComponent
  ],
  entryComponents: [
    // ????
  ]
})
export class NgCoreLinkModule {
  static forRoot(): ModuleWithProviders<NgCoreLinkModule> {
    return {
      ngModule: NgCoreLinkModule,
      providers: [
        CommonLinkService,
        PermalinkManagerService
      ]
    };
  }
}
