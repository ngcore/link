import { DevLogger as dl } from '@ngcore/core'; import isDL = dl.isLoggable;
// import { DateIdUtil } from '@ngcore/core';


export namespace CommonLinkUtil {

  // TBD:
  export function buildImageUrl(imgPrefix: string, image: string): string {
    // tbd:
    if (!image) {
      return '';   // ???
    }
    if (!imgPrefix) {
      return image;
    }
    // Exclude absolute urls.
    if (image.startsWith('http') || image.startsWith('/')) {
      return image;
    } else {
      if (!imgPrefix.endsWith('/')) {
        imgPrefix += '/';
      }
      let parentPrefix = null;
      let x = imgPrefix.substring(0, imgPrefix.length - 2).lastIndexOf('/');
      if (x != -1) {
        parentPrefix = imgPrefix.substring(0, x + 1);
      }

      let imgUrl: string;
      if (image.startsWith('../')) {  // Supports one-level up parent folder.
        if (parentPrefix) {
          imgUrl = parentPrefix + image.substring(3);
        } else {
          // ???
          imgUrl = image.substring(2);   // ???
        }
      } else {
        imgUrl = imgPrefix + image;
      }
      if(isDL()) dl.log(`buildImageUrl(): imgUrl = ${imgUrl} from imgPrefix = ${imgPrefix}; image = ${imgPrefix}`);
      return imgUrl;
    }
  }

}
