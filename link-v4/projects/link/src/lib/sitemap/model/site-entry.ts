import { DevLogger as dl } from '@ngcore/core'; import isDL = dl.isLoggable;
import { DateTimeUtil, DateIdUtil } from '@ngcore/core';
import { ChangeFrequency } from '../core/change-frequency';


export class SiteEntry {

  // Used when printing lastModified.
  // By making this a static variable,
  //    we cannot have a mixed listing (e.g., some with full date-time string and others with date only).
  //    But, it's more convenient this way.
  // --> Well we can also include it as an instance variable.
  public static excludeTimePart: boolean = false;

  constructor(
    public location: string = '',
    public lastModified: number = 0,  // Unix epoch time millis. 0 means unknown.
    public changeFrequency: ChangeFrequency = ChangeFrequency.never,
    public pagePriority: number = 0.5,
    public excludeTime: boolean = SiteEntry.excludeTimePart
  ) {
    // if(pagePriority < 0) {
    //   this.pagePriority = 0;
    // } else if(pagePriority > 1.0) {
    //   this.pagePriority = 1.0;
    // }
  }

  get loc(): string {
    return this.location;
  }
  get lastMod(): string {
    return DateTimeUtil.getISODateTimeString(this.lastModified, this.excludeTime);
  }
  get changeFreq(): string {
    return ChangeFrequency[this.changeFrequency];
  }
  get priority(): string {
    if (this.pagePriority <= 0) {
      return "0";
    } else if (this.pagePriority >= 1.0) {
      return "1.0";
    }
    let truncated = Math.floor(this.pagePriority * 10) / 10;
    return truncated.toString();
  }


  // TBD:
  toXML(indent: number = 2): string {
    let xml = `  <url>
    <loc>${this.loc}</loc>
    <lastmod>${this.lastMod}</lastmod>
    <changefreq>${this.changeFreq}</changefreq>
    <priority>${this.priority}</priority>
  </url>`;
    return xml;
  }


  toString(): string {
    return ''
      + 'location = ' + this.location
      + '; lastModified = ' + this.lastModified
      + '; changeFrequency = ' + this.changeFrequency
      + '; pagePriority = ' + this.pagePriority;
  }

  clone(): SiteEntry {
    let cloned = Object.assign(new SiteEntry(), this) as SiteEntry;
    return cloned;
  }
  static clone(obj: any): SiteEntry {
    let cloned = Object.assign(new SiteEntry(), obj) as SiteEntry;
    return cloned;
  }


}
