export enum ChangeFrequency {
  never = 0,
  always,
  hourly,
  daily,
  weekly,
  monthly,
  yearly
}
