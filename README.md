# @ngcore/link
> NG Core angular/typescript library for URLs.

Link-related helper/utility library for Angular.
(Current version requires Angular v7.2+)


## API Docs

* http://ngcore.gitlab.io/link/



